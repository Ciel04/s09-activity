package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@SpringBootApplication
@RestController
// The @RestController annotation tells springboot that this application will function as an endpoint that will be used in handling web request.
@RequestMapping("/greeting")
// Will require all routes within the DiscussionApplication 
public class DiscussionApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}


	@GetMapping("/hello")
	//Maps a get request to the route /hello and the method hello
	public String hello() {
		return "Hello World!";
	}

	//Routes with a string query
	//Dynamic data is obtained from URL's string query
	//"%s" Specifies that the value included in the format is of any type
	//localhost:8080/hi?name="Jim
	@GetMapping("/hi")
	public String hi(@RequestParam(value = "name", defaultValue = "John") String name) {
		return String.format("Hi %s", name);
	}

	//Multiple Parameters
	//localhost:8080/friend?name=Ask&friend=Pikachu
	@GetMapping("/friend")
	public String friend(@RequestParam(value = "name", defaultValue = "John") String name, @RequestParam(value = "friend", defaultValue = "Jane") String friend) {
		return String.format("Hello %s, My name is %s.", friend, name);
	}

	//Routes with path variables
	@GetMapping("/hello/{name}")
	//localhost:8080/hello/joe
	//PathVariable annotation allows us to extract data directly from the url
	public String courses(@PathVariable("name") String name) {
		return String.format("Nice to meet you %s", name);
	}

	//ACTIVITY
	public ArrayList<String> enrollees = new ArrayList<>();
	@GetMapping("/enroll")
	public String getUser(@RequestParam(value = "user", defaultValue = "John") String user) {
		enrollees.add(user);
		return String.format("Thank you for enrolling, %s!", user);
	}
	@GetMapping("/getEnrollees")
	public String getEnrollees() {
		return enrollees.toString();
	}
	@GetMapping("/nameage")
	public String getNameAge(@RequestParam(value = "name", defaultValue = "John") String name, @RequestParam(value = "age", defaultValue = "17") String age) {
		return String.format("Hello %s! My age is %s.", name, age);
	}
	@GetMapping("/course/{id}")
	public String getCourse(@PathVariable String id) {
		String msg="";
		switch (id){
			case "java101":
				msg = "Name: Java 101, Schedule: MWF 8:00 AM - 11:00 AM, Price: PHP 3000";
				break;
			case "sql101":
				msg = "Name: SQL 101, Schedule: MWF 1:00 PM - 3:00 PM, Price: PHP 3300";
				break;
			case "javaee101":
				msg = "Name: Java EE 101, Schedule: TTH 8:00 AM - 11:00 AM, Price: PHP 5500";
				break;
			default:
				msg = "Cannot be found";
		}
		return msg;
	}
}
